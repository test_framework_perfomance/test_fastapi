from pydantic import BaseModel


class _TestDataItem(BaseModel):
    data: str


class TestDataInput(BaseModel):
    data_input: list[_TestDataItem]
