from typing import Any

from pydantic import BaseModel


class _ResponseMeta(BaseModel):
    msg: str


class ResponseScheme(BaseModel):
    data: dict
    meta: _ResponseMeta


class _TestDataItem(BaseModel):
    index: int
    data: str


class ResponseTestData(ResponseScheme):
    data: list[_TestDataItem]
