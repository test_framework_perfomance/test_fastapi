import os
import pathlib
from typing import Any

from pydantic_settings import BaseSettings, SettingsConfigDict

_base_to_env = pathlib.Path(__file__).parents[1] / 'dc_fastapi_test' / '.env'
if _base_to_env.exists():
    from dotenv import load_dotenv

    load_dotenv(_base_to_env, override=True)


class MyBaseSettings(BaseSettings):
    model_config = SettingsConfigDict(
        env_file='.env', env_file_encoding='utf-8', extra="ignore"
    )


class DBConfig(MyBaseSettings):
    model_config = SettingsConfigDict(env_prefix="BACKEND_DB_")

    HOST: str
    USER: str
    PASSWORD: str
    NAME: str
    PORT: int = 5432

    def get_config_data(self) -> dict[str, Any]:
        return {
            'host': self.HOST,
            'user': self.USER,
            'password': self.PASSWORD,
            'database': self.NAME,
            'port': self.PORT,
        }

    def get_config_str(self, driver: str) -> str:
        return f'{driver}://{self.USER}:{self.PASSWORD}@{self.HOST}:{self.PORT}/{self.NAME}'


connection_pool = int(os.getenv('BACKEND_DB_POOL_SIZE', 10))

test_count_lines = int(os.getenv('TEST_COUNT_LINES', 10_000))
test_column_in_json = int(os.getenv('TEST_COUNT_COLUMN_IN_JOSN'))


db_config = DBConfig()
