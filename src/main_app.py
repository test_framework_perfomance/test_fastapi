from fastapi import FastAPI
from fastapi.applications import FastAPI as fast_app

from routers import router_api


def get_main_app() -> fast_app:
    my_app = FastAPI()
    my_app.include_router(router_api)
    return my_app


app = get_main_app()
