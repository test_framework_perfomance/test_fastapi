from fastapi import APIRouter

from routers.v1 import api_v1

router_api = APIRouter(prefix='/api')
router_api.include_router(api_v1)
