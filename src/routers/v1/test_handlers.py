from http import HTTPStatus

from fastapi import APIRouter, Body

from repos.postgres import get_repo_postgres
from schemes.request import TestDataInput
from schemes.response import ResponseTestData
from utils.response_utility import get_response

api_tests = APIRouter(prefix='/test_data', tags=['Тестовые ручки'])


@api_tests.get('', response_model=ResponseTestData)
async def get_data(from_index: int | None = 0, batch_size: int | None = 100):
    try:
        repo_postgres = await get_repo_postgres()
        list_data = await repo_postgres.get_batch_data(
            from_index=from_index, batch_size=batch_size
        )
        return get_response(data={'data': list_data}, status=HTTPStatus.OK)
    except Exception as exc_info:
        return get_response(
            meta={'msg': str(exc_info)}, status=HTTPStatus.INTERNAL_SERVER_ERROR
        )


@api_tests.post('', response_model=ResponseTestData)
async def add_data(insert_data: TestDataInput = Body(...)):
    try:
        repo_postgres = await get_repo_postgres()
        count_insert = await repo_postgres.add_data(input_data=insert_data)
        return get_response(
            meta={'msg': f'Добавлено {count_insert}'}, status=HTTPStatus.OK
        )
    except Exception as exc_info:
        return get_response(
            meta={'msg': str(exc_info)}, status=HTTPStatus.INTERNAL_SERVER_ERROR
        )
