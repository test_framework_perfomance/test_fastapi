from http import HTTPStatus

from fastapi import APIRouter

from repos.postgres import get_repo_postgres
from schemes.response import ResponseScheme
from utils.response_utility import get_response

manipulate_test_data = APIRouter(
    tags=['Работа с тестовыми данными'], prefix='/manipulate'
)


@manipulate_test_data.post('/generate', response_model=ResponseScheme)
async def generate_test_data(
    count_lines: int | None = 10_000, count_columns: int | None = 10
):
    try:
        repo_postgres = await get_repo_postgres()
        await repo_postgres.generate_data(
            count_lines=count_lines, count_columns=count_columns
        )
        return get_response(
            meta={'msg': 'Данные успешно созданы'}, status=HTTPStatus.OK
        )
    except Exception as exc_info:
        return get_response(
            meta={'msg': str(exc_info)}, status=HTTPStatus.INTERNAL_SERVER_ERROR
        )


@manipulate_test_data.post('/clear', response_model=ResponseScheme)
async def clear_test_data():
    try:
        repo_postgres = await get_repo_postgres()
        await repo_postgres.clear_table()
        return get_response(
            meta={'msg': 'Данные успешно очищены'}, status=HTTPStatus.OK
        )
    except Exception as exc_info:
        return get_response(
            meta={'msg': str(exc_info)}, status=HTTPStatus.INTERNAL_SERVER_ERROR
        )
