from fastapi import APIRouter

from routers.v1.test_data_manipulation import manipulate_test_data
from routers.v1.test_handlers import api_tests

api_v1 = APIRouter(prefix='/v1')
api_v1.include_router(manipulate_test_data)
api_v1.include_router(api_tests)
