import asyncio
import pathlib

import config
from repos.postgres import get_repo_postgres

PATH_TO_SERVICE_FILE = pathlib.Path('/app/volumes/data_is_created.txt')


async def init_action() -> None:
    if PATH_TO_SERVICE_FILE.exists():
        print('start init skipped')
        return
    print('start init')
    repo = await get_repo_postgres()
    await repo.generate_data(
        count_lines=config.test_count_lines, count_columns=config.test_column_in_json
    )
    with PATH_TO_SERVICE_FILE.open('w') as f:
        f.write('')


if __name__ == '__main__':
    asyncio.run(init_action())
