from http import HTTPStatus
from typing import Any

from fastapi.responses import JSONResponse


def get_response(
    data: dict[str, Any] | None = None,
    meta: dict[str, Any] | None = None,
    status: int = HTTPStatus.OK,
) -> JSONResponse:
    return JSONResponse(
        content={'data': data or {}, 'meta': meta or {'msg': ''}}, status_code=status
    )
