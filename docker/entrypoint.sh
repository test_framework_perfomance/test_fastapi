#!/bin/bash
python init_script.py
gunicorn -k uvicorn.workers.UvicornWorker main_app:app --preload --max-requests 1200 --max-requests-jitter 300 --bind 0.0.0.0:8000 --workers 4
